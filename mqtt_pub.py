import paho.mqtt.client as mqtt
import random
import schedule
import time

mqttc = mqtt.Client("python_pub")
mqttc.connect("localhost", 1883)
#mqttc.publish("hello/world", "Hello, World")
def job():
	mqttc.publish("hello/world", random.randint(1, 10))

schedule.every(1).seconds.do(job)

while True:
	schedule.run_pending()
	time.sleep(1)

mqttc.loop(2)